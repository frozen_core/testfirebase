﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class DataPayload {
	public static DataPayload current;
	public UserModel user;
	public List<RecordsModel> bestRecords;
	public int bestScore;
	public DataPayload()
	{
		user = new UserModel();
		bestRecords = new List<RecordsModel>();
		bestScore = 0;
	}
}
