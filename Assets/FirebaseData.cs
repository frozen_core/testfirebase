﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine;
public class FirebaseData {
	static bool userWasLoged;
	private FirebaseData()
	{
		PersistentData.Load();
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(url);
		reference = FirebaseDatabase.DefaultInstance.RootReference;
		if(FirebaseAuth.DefaultInstance.CurrentUser != null)
		{
			users = reference
				.Child("users")
				.OrderByChild("username");
			//TODO: This is only for test, remove it
			users.GetValueAsync().ContinueWith(data =>{
				DataSnapshot dataSnapshot = data.Result;
				listUser = new List<UserModel>();
				foreach(DataSnapshot child in dataSnapshot.Children)
				{
					listUser.Add(JsonUtility.FromJson<UserModel>(child.GetRawJsonValue());
				}
			});
			//
			bestRecords = reference
				.Child("galacticrider")
				.OrderByChild("bestRecord")
				.LimitToLast(5);
			bestRecords.ValueChanged += RefreshRecords;
			BestRecords = DataPayload.current.bestRecords;

			bestRecordQuery = reference
				.Child("galacticrider")
				.OrderByChild("username")
				.EqualTo(FirebaseAuth.DefaultInstance.CurrentUser.DisplayName);
			bestRecordQuery.ValueChanged += RefreshRecord;
			bestRecord = DataPayload.current.bestScore;

			userWasLoged = true;
		}
		else
		{
			userWasLoged = false;
		}
	}
	readonly static string url = "https://frozen-core.firebaseio.com/";
	public static FirebaseData GetInstance()
	{
		if(instance == null || ((userWasLoged && FirebaseAuth.DefaultInstance.CurrentUser == null) || (!userWasLoged && FirebaseAuth.DefaultInstance.CurrentUser != null)))
		{
			instance = new FirebaseData();
		}
		return instance;
	}
	public Task<DataSnapshot> UserAvailable(string displayName)
	{
		return users.EqualTo(displayName).GetValueAsync();
	}
	static void RefreshRecords(object sender, ValueChangedEventArgs args)
	{
		DataSnapshot snapshot = args.Snapshot;
		instance.BestRecords = (snapshot.Children.AsQueryable().Select(x => JsonUtility.FromJson<RecordsModel>(x.GetRawJsonValue())).ToList());
		DataPayload.current.bestRecords = instance.BestRecords.OrderByDescending(x => x.bestRecord).ToList();
		PersistentData.Save();
	}
	static void RefreshRecord(object sender, ValueChangedEventArgs args)
	{
		DataSnapshot snapshot = args.Snapshot;
		instance.bestRecord = JsonUtility.FromJson<RecordsModel>(snapshot.Children.AsQueryable().FirstOrDefault().GetRawJsonValue()).bestRecord;
		DataPayload.current.bestScore = instance.bestRecord;
		PersistentData.Save();
	}
	public static Task InsertUser(string UserId, string jsonValue)
	{
		return FirebaseDatabase.DefaultInstance.RootReference.Child("users").Child(UserId).SetRawJsonValueAsync(jsonValue);
	}
	public static Task InsertRecord(int newRecord)
	{
		string json = JsonUtility.ToJson(new RecordsModel() { username = FirebaseAuth.DefaultInstance.CurrentUser.DisplayName, bestRecord = newRecord }); 
		return FirebaseDatabase.DefaultInstance.RootReference.Child("galacticrider").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId).SetRawJsonValueAsync(json);
	}
	private static FirebaseData instance;
	public List<RecordsModel> records;
	public int bestRecord;
	private static Query users;
	private static Query bestRecordQuery;
	private static Query bestRecords;
	public List<UserModel> listUser;
	public List<RecordsModel> BestRecords;
	private DatabaseReference reference;
}
