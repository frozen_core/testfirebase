﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using System.Linq;

public class GetData : MonoBehaviour {
	FirebaseAuth auth;
	public Text Authorization;
	public Text Users;
	public Text GalacticRider;
	public InputField NewRecord;
	void Start () {
		auth = FirebaseAuth.DefaultInstance;
		auth.SignInWithEmailAndPasswordAsync("frozencoreent@gmail.com", "Test123").ContinueWith(login =>
			{
				if(login.IsCompleted)
				{
					FirebaseUser user = login.Result;
					Authorization.text = 
						user.Email + Environment.NewLine +
						user.DisplayName + Environment.NewLine +
						user.UserId + Environment.NewLine;
				}
			}
		);
	}
	void Update () {
		Users.text = string.Join(Environment.NewLine, FirebaseData.GetInstance().listUser.Select(x => x.username + "\t" +x.email).ToArray());
		GalacticRider.text = string.Join(Environment.NewLine, FirebaseData.GetInstance().BestRecords.Select(x => x.username + "\t" + x.bestRecord).ToArray());
	}
	public void Add()
	{
		FirebaseData.InsertRecord(int.Parse(NewRecord.text));
	}
}
