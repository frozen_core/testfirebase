﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class PersistentData {
    public static DataPayload savedData;

    public static void Save()
    {
        savedData = DataPayload.current;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Path.Combine(Application.persistentDataPath , "file.file"));
        bf.Serialize(file, PersistentData.savedData);
        file.Close();
    }
	public static void Load() {
		if(File.Exists(Path.Combine(Application.persistentDataPath, "file.file"))) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Path.Combine(Application.persistentDataPath , "file.file"), FileMode.Open);
			PersistentData.savedData = (DataPayload)bf.Deserialize(file);
			file.Close();
		}
        else
        {
            PersistentData.savedData = new DataPayload();
        }
        DataPayload.current = PersistentData.savedData;
	}
}
