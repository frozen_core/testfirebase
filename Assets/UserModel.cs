﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class UserModel {
    public string username;
    public string email;
    public UserModel() {
    }

    public UserModel(string username, string email) {
        this.username = username;
        this.email = email;
    }
}
